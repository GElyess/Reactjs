import GameComponent from "../components/GameComponent";

const Game = () => {
    return(<div>
        <GameComponent />
    </div>);
}

export default Game;