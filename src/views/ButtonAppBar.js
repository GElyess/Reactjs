import React from "react";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import { Link } from "react-router-dom";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";

export default function ButtonAppBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div className="top-menu-bar">
      <div>
          <IconButton
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={(e) => setAnchorEl(e.currentTarget)}
            edge="start"
            color="inherit"
            aria-label="menu"
          >
            <MenuIcon />
          </IconButton>
        </div>

          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={() => setAnchorEl(null)}
          >
            <MenuItem onClick={handleClose} component={Link} to={"/about"}>
              About
            </MenuItem>
            <MenuItem onClick={handleClose} component={Link} to={"/cv"}>
              CV
            </MenuItem>
            <MenuItem onClick={handleClose} component={Link} to={"/home"}>
              Home
            </MenuItem>
            <MenuItem onClick={handleClose} component={Link} to={"/apis"}>
              APIs
            </MenuItem>
            <MenuItem onClick={handleClose} component={Link} to={"/game"}>
              Game
            </MenuItem>
            <MenuItem onClick={handleClose} component={Link} to={"/sms"}>
              SMS
            </MenuItem>
          </Menu>
          <div className="top-menu-bar-label" >
            GElyess
          </div>
          <IconButton
            aria-controls="login"
            aria-label="login"
            onClick={(e) => console.log("placeholder click")}
            edge="start"
            color="inherit"
          >
            <MenuIcon />
          </IconButton>
    </div>
  );
}
