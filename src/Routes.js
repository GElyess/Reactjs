import { Route } from "react-router-dom";
import ButtonAppBar from "./views/ButtonAppBar";
import Home from "./views/Home";
import Game from "./views/Game";
import Apis from "./views/Apis";
import P404 from "./views/404page"
import Cv from "./views/Cv";
import About from "./views/About";
import Sms from "./views/Sms";
import { Switch } from "react-router-dom";

const Routes = () => {
  return (
    <div className="master-container">
      <div className="toolbar">
        <ButtonAppBar />
        <Switch>
          <Route path="/game" exact component={Game} />
          <Route path="/apis" exact component={Apis} />
          <Route path="/" exact component={Home} />
          <Route path="/home" exact component={Home} />
          <Route path="/sms" exact component={Sms} />
          <Route path="/cv" exact component={Cv} />
          <Route path="/about" exact component={About} />
          <Route component={P404} />
        </Switch>
      </div>
    </div>
  );
};

export default Routes;
