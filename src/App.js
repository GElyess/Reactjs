import Routes from './Routes'
import {BrowserRouter as Router} from "react-router-dom";
// import "./css/root.css"
const App = () => {
  return(
    <>
    <Router>
      <Routes/>
    </Router>
    </>
    );
}
export default App;