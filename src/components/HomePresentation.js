import React from "react";
import { useState } from "react";
import { Link } from "react-router-dom";

const HomePresentation = () => {
  return (
      <ol>
        <Item
          text="It's a game where the goal is to beat the set resistance in a clicking battle"
          present={"Game"}
        />
        <Item
          text="Trigger some fun apis by clicking the buttons"
          present={"Apis"}
        />
        <Item 
          text="Send me an SMS by filling the form" 
          present={"SMS"} 
          />
      </ol>
  );
};

function Item({ text, present }) {
  const [isOpen, setIsOpen] = useState(false);

  const toggleOpen = () => setIsOpen(!isOpen);

  return (
    <li layout onClick={toggleOpen} initial={{ borderRadius: "5px "}}>
      <div className="avatar">
        {present}
      </div>
        {isOpen && <Content text={text} present={present} />}
    </li>
  );
}

const Content = ({ text, present }) => {
  return (
    <div>
      <div className="row">
        {text}
        <Link to={`/${present}`}>{` ${present}.`}</Link>{" "}
      </div>
    </div>
  );
};

export default HomePresentation;
