import { useState } from "react";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import MenuBookIcon from "@material-ui/icons/MenuBook";

const DrawerComponent = ({ mlt, add, res }) => {
  const [state, setState] = useState({
    top: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    // if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
    //   return;
    // }

    setState({ ...state, [anchor]: open });
  };

  const adjustLevel = (index) => {
    const difficulty = [
      {
        Difficulty: "auto",
        InitialResistance: -1,
        InitialMultiplier: 4.2,
        InitialAdditive: 5.3,
      },
      {
        Difficulty: "toddler",
        InitialResistance: 2,
        InitialMultiplier: 1.2,
        InitialAdditive: 2.1,
      },
      {
        Difficulty: "easy",
        InitialResistance: 5,
        InitialMultiplier: 1,
        InitialAdditive: 2.5,
      },
      {
        Difficulty: "normal",
        InitialResistance: 5,
        InitialMultiplier: 1,
        InitialAdditive: 2.5,
      },
      {
        Difficulty: "hard",
        InitialResistance: 5,
        InitialMultiplier: 1,
        InitialAdditive: 2.5,
      },
      {
        Difficulty: "hell",
        InitialResistance: 5,
        InitialMultiplier: 1,
        InitialAdditive: 1,
      },
    ];
    add(difficulty[index].InitialAdditive);
    mlt(difficulty[index].InitialMultiplier);
    res(difficulty[index].InitialResistance);
  };
  const list = () => (
    <div>
      <List>
        {["Auto", "Toddler", "Easy", "Normal", "Hard", "Hell"].map(
          (text, index) => (
            <ListItem
              button
              key={text}
              onClick={() => {
                adjustLevel(index);
              }}
            >
              <ListItemText primary={text} />
            </ListItem>
          )
        )}
      </List>
      <Divider />
      <List>
        {["Home", "Eh ....", "About"].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <>
      <button className="bubbly-button bubbly-button-sub"
        onClick={toggleDrawer("top", true)}
      >
        <MenuBookIcon />
        </button>
      <Drawer
        anchor={"top"}
        open={state["top"]}
        onClose={toggleDrawer("top", false)}
      >
        {list("top")}
      </Drawer>
    </>
  );
};

export default DrawerComponent;
