import Axios from "axios";
import Button from "@material-ui/core/Button";
import { useState } from "react";
import { Fragment } from "react";

const FetchAPI = () => {
  const [text, setText] = useState("Nothing");
  const token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0MzcxNCIsImlhdCI6MTYxOTk3MDA4OH0.l9LiIXyv8EzG6-Ajnhv9x_FELTBLcRvSnxLM4APsyBI";
  const SmsFactor = async () => {
    const response = await Axios.get(
      "https://api.smsfactor.com/send?text=hahaha world&to=33778352777",
      {
        headers: {
          Host: "api.smsfactor.com",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    setText(JSON.stringify(response.data));
  };

  const JokeAPI = async () => {
    const response = await Axios.get(
      "https://official-joke-api.appspot.com/random_joke"
    );
    setText(response.data.setup + "..." + response.data.punchline);
  };

  const AdviceAPI = async () => {
    const randomize = Math.floor(Math.random() * 217) + 1;
    const response = await Axios.get(
      "https://api.adviceslip.com/advice/" + randomize
    );

    setText(JSON.parse(response.data + "}").slip.advice);
  };

  return (
    <Fragment>
      {text}
      <Button onClick={SmsFactor}>Generate an SMS</Button>
      <Button onClick={JokeAPI}>Generate a joke</Button>
      <Button onClick={AdviceAPI}>Generate an advice</Button>
    </Fragment>
  );
};

export default FetchAPI;
