import { useState } from "react";
import { Grid } from "@material-ui/core/";
import GaugeChart from "react-advanced-gauge-chart";
import ModalComponent from "./ModalComponent";
import Settings from "./Settings";
import useCountDown from "react-countdown-hook";
import { Fragment } from "react";

const GameComponent = () => {
  const [value, setValue] = useState(0);
  const [timeLeft, { start, reset, pause }] = useCountDown(100000, 100);

  return (
    <Fragment>
      <Grid container spacing={3}>
        <Grid item sm={12}>
          <GaugeChart
            id="gauge-chart2"
            percent={value}
            style={{width:"90vw"}}
            previousValue={1}
            nrOfLevels={12}
          />
        </Grid>
        <Grid item sm={12}>
          <Settings
            timeLeft={timeLeft}
            value={value}
            makeValue={setValue}
            start={start}
            reset={reset}
          />
        </Grid>
      </Grid>
      <ModalComponent
        value={value}
        minMessage="Loose"
        maxMessage="Win"
        timeOfFinish={timeLeft / 1000}
        stopTime={pause}
      />
    </Fragment>
  );
};
export default GameComponent;
