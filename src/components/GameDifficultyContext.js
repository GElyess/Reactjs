import React, { useContext } from "react";

const GameDifficultyContext = React.createContext();
const Modes = [
  {
    Difficulty: "auto",
    InitialResistance: 0,
    InitialMultiplier: 4.2,
    InitialAdditive: 5.3,
  },
  {
    Difficulty: "toddler",
    InitialResistance: 2,
    InitialMultiplier: 1.2,
    InitialAdditive: 2.1,
  },
  {
    Difficulty: "easy",
    InitialResistance: 5,
    InitialMultiplier: 1,
    InitialAdditive: 2.5,
  },
  {
    Difficulty: "normal",
    InitialResistance: 5,
    InitialMultiplier: 1,
    InitialAdditive: 2.5,
  },
  {
    Difficulty: "hard",
    InitialResistance: 5,
    InitialMultiplier: 1,
    InitialAdditive: 2.5,
  },
  {
    Difficulty: "hell",
    InitialResistance: 5,
    InitialMultiplier: 1,
    InitialAdditive: 1,
  },
];

export function useDifficulty() {
  return useContext(GameDifficultyContext);
}

export function GameDifficultyProvider({ children }) {
  return (
    <GameDifficultyContext.Provider value={"WTF IS THIS DAMN SHIT MATE"}>
      {children}
    </GameDifficultyContext.Provider>
  );
}
