import { forwardRef, useState, useEffect } from "react";
import PropTypes from "prop-types";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";

const Fade = forwardRef(function Fade(props, ref) {
  const { in: open, children, onEnter, onExited, ...other } = props;

  return (
    <div ref={ref} {...other}>
      {children}
    </div>
  );
});

Fade.propTypes = {
  children: PropTypes.element,
  in: PropTypes.bool.isRequired,
  onEnter: PropTypes.func,
  onExited: PropTypes.func,
};

export default function SpringModal({
  value,
  minMessage,
  maxMessage,
  timeOfFinish,
  stopTime,
}) {
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("Neutral");

  useEffect(() => {
    if (value >= 1) {
      setMessage(maxMessage);
      stopTime();
      handleOpen();
    } else if (value < 0) {
      setMessage(minMessage);
      stopTime();
      handleOpen();
    }
  }, [value, maxMessage, minMessage, stopTime]);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
      <Modal
        aria-labelledby="spring-modal-title"
        aria-describedby="spring-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >

        <Fade in={open}>
          <div className="modal-component">
            <div className="modal-title" id="spring-modal-title"><b>You {message}</b></div>
            <p id="spring-modal-description">Time: {timeOfFinish} seconds</p>
          </div>
        </Fade>

      </Modal>
  );
}
