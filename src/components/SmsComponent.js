import MailOutlineIcon from "@material-ui/icons/MailOutline";
import EventAvailableRoundedIcon from "@material-ui/icons/EventAvailableRounded";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import { Input } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import { Fragment } from "react";
import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import DoneOutlineRoundedIcon from "@material-ui/icons/DoneOutlineRounded";
import ClearRoundedIcon from "@material-ui/icons/ClearRounded";
import Axios from "axios";


const SmsComponent = () => {
  const [sender, setSender] = useState("Blonde, James Blonde");
  const [message, setMessage] = useState("Hello Elyess :D");
  const [number, setNumber] = useState("33778352777");
  const [date, setDate] = useState();
  const [delay, setDelay] = useState(false);
  const [smsApi, setSmsApi] = useState("Nothing yet ...");
  const [state, setState] = useState({
    ActivateSender: true,
    ActivateMessage: true,
    ActivateNumber: true,
    ActivateDelay: true,
  });

  const token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI0MzcxNCIsImlhdCI6MTYxOTk3MDA4OH0.l9LiIXyv8EzG6-Ajnhv9x_FELTBLcRvSnxLM4APsyBI";
  const accessKey = "ec705b1d1ef5395270ac4f74ea916268";
  const PhoneNumCheck = async () => {
    const response = await Axios.get(
      `http://apilayer.net/api/validate?access_key=${accessKey}&number=${number}&format=1`,
      {
        headers: {
          Accept: "application/json",
        },
      }
    );
    setSmsApi(JSON.stringify(response.data, null, "\t"));
  };

  const SmsFactor = async () => {
    const response = await Axios.get(
      delay && date
        ? `https://api.smsfactor.com/send?text=from: ${sender} -------> ${message}&to=${number}&delay=` +
            date.replace("T", " ")
        : `https://api.smsfactor.com/send?text=from: ${sender} -------> ${message}&to=${number}`,
      {
        headers: {
          Host: "api.smsfactor.com",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
        },
      }
    );
    setSmsApi(JSON.stringify(response.data, null, "\t"));
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });

    if (event.target.checked) {
      if (event.target.name === "ActivateSender") setSender(sender);
      else if (event.target.name === "ActivateMessage") setMessage(message);
      else if (event.target.name === "ActivateNumber") setNumber(number);
      else if (event.target.name === "ActivateDelay") setDelay(true);
    } else {
      if (event.target.name === "ActivateSender") setSender("Anonymous");
      else if (event.target.name === "ActivateMessage")
        setMessage("Hello Elyess :D");
      else if (event.target.name === "ActivateNumber") setNumber("33778352777");
      else if (event.target.name === "ActivateDelay") setDelay(false);
    }
    console.log(date ? date.replace("T", " ") : "");
  };
  return (
    <Fragment>
      <Box display="flex" alignItems="flex-start" flexWrap="wrap" margin="10px">
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                icon={<ClearRoundedIcon />}
                checkedIcon={<DoneOutlineRoundedIcon />}
                name="ActivateSender"
                color="primary"
                onChange={handleChange}
              ></Checkbox>
            }
            label="Sender"
          />

          <Input
            value={sender}
            onChange={(e) => {
              setSender(e.target.value);
            }}
          />
        </Box>
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                icon={<ClearRoundedIcon />}
                checkedIcon={<DoneOutlineRoundedIcon />}
                name="ActivateMessage"
                color="primary"
                onChange={handleChange}
              ></Checkbox>
            }
            label="Message"
          />
          <Input
            value={message}
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
        </Box>
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                icon={<ClearRoundedIcon />}
                checkedIcon={<DoneOutlineRoundedIcon />}
                name="ActivateNumber"
                color="primary"
                onChange={handleChange}
              ></Checkbox>
            }
            label="Number"
          />
          <Input
            value={number}
            onChange={(e) => {
              setNumber(e.target.value);
            }}
          />
        </Box>
        <Box>
          <FormControlLabel
            control={
              <Checkbox
                defaultValue={false}
                icon={<EventAvailableRoundedIcon />}
                checkedIcon={<EventAvailableIcon />}
                name="ActivateDelay"
                color="primary"
                onChange={handleChange}
              ></Checkbox>
            }
            label="Delay Message"
          />

          <TextField
            id="datetime-local"
            label="Specify date"
            type="datetime-local"
            defaultValue="2021-04-20T10:42:42"
            InputLabelProps={{
              shrink: true,
            }}
            onChange={(e) => {
              setDate(e.target.value);
            }}
          />
        </Box>
      </Box>

      <div
      >
        <Button
          color="primary"
          variant="outlined"
          startIcon={<MailOutlineIcon />}
          endIcon={<MailOutlineIcon />}
          onClick={(e) => SmsFactor()}
        >
          Trigger SMS
        </Button>
      </div>

      <div
      >
        <Button
          color="primary"
          variant="outlined"
          startIcon={<MailOutlineIcon />}
          endIcon={<MailOutlineIcon />}
          onClick={(e) => PhoneNumCheck()}
        >
          Trigger Phone number check
        </Button>
      </div>
      {/* {` sender:${sender} message:${message} number:${number} delay: ${delay} date: ${date}`} */}

      <div >
        <Box>{`Hover the JSON response: ${smsApi}`}</Box>{" "}
      </div>
    </Fragment>
  );
};

export default SmsComponent;
