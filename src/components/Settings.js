import React, { useState, useEffect } from "react";
import FingerprintIcon from "@material-ui/icons/Fingerprint";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Favorite from "@material-ui/icons/Favorite";
import Checkbox from "@material-ui/core/Checkbox";
import { Input } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import DrawerComponent from "./DrawerComponent";

const Settings = ({ timeLeft, value, makeValue, start, reset, pause }) => {
  const [multiplier, setMultiplier] = useState(1);
  const [additive, setAdditive] = useState(1);
  const [resistance, setResistance] = useState(1);
  const [initial, setInitial] = useState(true);
  const [timer, setTimer] = useState(40000);
  const [seconds, setSeconds] = useState(0);
  const [text, setText] = useState("Start ?");
  const [state, setState] = useState({
    ActivateTimer: true,
    ActivateResistance: true,
    ActivateMultiplier: true,
    ActivateAdditive: true,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });

    if (event.target.checked) {
      if (event.target.name === "ActivateTimer") setTimer(timer);
      else if (event.target.name === "ActivateResistance")
        setResistance(resistance);
      else if (event.target.name === "ActivateMultiplier")
        setMultiplier(multiplier);
      else if (event.target.name === "ActivateAdditive") setAdditive(additive);
    } else {
      if (event.target.name === "ActivateTimer") setTimer(99999);
      else if (event.target.name === "ActivateResistance") setResistance(0);
      else if (event.target.name === "ActivateMultiplier") setMultiplier(1);
      else if (event.target.name === "ActivateAdditive") setAdditive(5);
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      setSeconds((seconds) => seconds + 1);
    }, 1000);
    if (+value > 0 && state.ActivateResistance) {
      console.log("value is :", value);
      const synch = () => makeValue(+value - +resistance / 100);
      synch();
      setText("Progression " + Math.trunc(+value * 100 - +resistance) + " %");
      console.log("Resistance Proc:", +value * 100 - +resistance);
    }

    return () => clearInterval(interval);
  }, [seconds]);

  const Reset = (event) => {
    makeValue(0);
    setTimer(timer);
    reset(timer);
    setText("Again ?");
    setInitial(true);
  };

  const FormatSeconds = (ms) => {
    var minutes = Math.floor(ms / 60000);
    var seconds = ((ms % 60000) / 1000).toFixed(0);
    return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
  };

  const Update = (flag) => {
    if (initial && !flag) {
      start(timer);
      setInitial(false);
    } else {
      if (flag && +value > 0 && state.ActivateResistance) {
        console.log("value is :", value);
        const synch = () => makeValue(+value - +resistance / 100);
        synch();
        setText("Progression " + Math.trunc(+value * 100 - +resistance) + " %");
        console.log("Resistance Proc:", +value * 100 - +resistance);
      }
      if (!flag) {
        const synch = () => makeValue(+value + (+additive * +multiplier) / 100);
        synch();
        setText(
          "Progression " +
            Math.trunc((+value + (+additive * +multiplier) / 100) * 100) +
            " %"
        );
        console.log("Click triggered value:", value);
        console.log(
          "flag:",
          flag,
          "value:",
          +value,
          "state.ActivateResistance:",
          state.ActivateResistance
        );
      }
    }
  };
  return (
    <div>
      <div>You have been here for {seconds} seconds</div>
      <div>Time left: {FormatSeconds(timeLeft)}</div>
      <FormControlLabel
        control={
          <Checkbox
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
            name="ActivateTimer"
            checked={state.ActivateTimer}
            color="primary"
            onChange={handleChange}
          ></Checkbox>
        }
        label="Timer (ms)"
      />
      <Input value={timer} onChange={(e) => setTimer(e.target.value)} />
      <FormControlLabel
        control={
          <Checkbox
            color="primary"
            icon={<FavoriteBorder />}
            checkedIcon={<Favorite />}
            name="ActivateResistance"
            checked={state.ActivateResistance}
            onChange={handleChange}
          ></Checkbox>
        }
        label="Resistance (%)"
      />
      <Input
        value={resistance}
        onChange={(e) => setResistance(e.target.value)}
      />
      <FormControlLabel
        control={
          <Checkbox
            color="primary"
            checkedIcon={<Favorite />}
            icon={<FavoriteBorder />}
            checked={state.ActivateMultiplier}
            name="ActivateMultiplier"
            onChange={handleChange}
          ></Checkbox>
        }
        label="Multiplier"
      />
      <Input
        value={multiplier}
        onChange={(e) => setMultiplier(e.target.value)}
      />
      <FormControlLabel
        control={
          <Checkbox
            checkedIcon={<Favorite />}
            icon={<FavoriteBorder />}
            name="ActivateAdditive"
            color="primary"
            checked={state.ActivateAdditive}
            onChange={handleChange}
          ></Checkbox>
        }
        label="Additive"
      />
      <Input value={additive} onChange={(e) => setAdditive(e.target.value)} />
      <div className="game-settings-button-group">
        <div className="bubbly-button-group">
          <button
            className="bubbly-button bubbly-button-main"
            onClick={(e) => Update(false)}
            onKeyPress={(e) =>
              e.key === " " || e.key === "s"
                ? Update(false)
                : e.key === "r"
                ? Reset(e)
                : ""
            }
          >
            <FingerprintIcon/>{text}<FingerprintIcon/>
          </button>
        </div>

        <button
          className="bubbly-button bubbly-button-sub"
          onClick={(e) => Reset(e)}
        >
          <DeleteIcon />
        </button>
        <DrawerComponent
          mlt={setMultiplier}
          add={setAdditive}
          res={setResistance}
        />
      </div>
    </div>
  );
};

export default Settings;
